#ifndef ETHERNET_HDR_H
#define ETHERNET_HDR_H

#include <sys/types.h>
#include <stdio.h>

/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN 6

/* Ethernet header */
///
/// \brief The sniff_ethernet struct defines the header of an
/// ethernet frame, and is used to parse the packet received
/// with pcap. Used to extract Ethernet layer information from a
/// packet.
///
struct sniff_ethernet {
        u_char  ether_dhost[ETHER_ADDR_LEN];    /**< destination host address / direccion destino */
        u_char  ether_shost[ETHER_ADDR_LEN];    /**< source host address / direccion fuente */
        u_short ether_type;                     /**< IP? ARP? RARP? etc */
};


#endif // ETHERNET_HDR_H

#ifndef UDP_HDR_H
#define UDP_HDR_H

///
/// \brief The sniff_tcp struct contains the UDP header.
/// We use the first two members to extract the src and dst port.
/// Used to extract TCP transport layer information from a packet.
struct sniff_udp {
        u_short th_sport;               /* source port */
        u_short th_dport;               /* destination port */
        u_short th_len ;
        u_short th_sum;                 /* checksum */
};
#endif // UDP_HDR_H
